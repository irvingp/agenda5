﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {

        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }




        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Resta()
        {
            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 4;
            int expected = -2;

            //Actuar
            int actual = SystemUnderTest.Resta(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, Valores no Coinciden");
        }

        [TestMethod]
        public void Multiplicacion()
        {
            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 6;

            //Actuar
            int actual = SystemUnderTest.Multiplicacion(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, Valores no Coinciden");
        }

        [TestMethod]
        public void Division()
        {
            //Arrange (Organizar)
            int value1 = 6;
            int value2 = 3;
            int expected = 2;

            //Actuar
            double actual = SystemUnderTest.Division(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual<double>(expected, actual, "Error, Valores no Coinciden");
        }

    }
}
